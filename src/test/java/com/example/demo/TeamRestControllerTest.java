package com.example.demo;

import com.example.demo.controller.TeamRestController;
import com.example.demo.entity.Team;
import com.example.demo.entity.TeamDTO;
import com.example.demo.service.TeamService;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;

import static org.hamcrest.Matchers.containsString;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@RunWith(SpringRunner.class)
@WebMvcTest(TeamRestController.class)
public class TeamRestControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private TeamService teamService;

    public String jsonify(Object o) throws JsonProcessingException {
        com.fasterxml.jackson.databind.ObjectWriter objectWriter = new ObjectMapper().writer().withDefaultPrettyPrinter();
        String jsonOutput = objectWriter.writeValueAsString(o);
        return  jsonOutput;
    }

    @Test
    public void shouldReturnDefaultMessage() throws Exception{
        when(teamService.getTeams()).thenReturn(Collections.emptyList());
        mockMvc.perform(get("/teams")).
                andDo(print()).
                andExpect(status().isOk()).andExpect(content().string(containsString("[]")));
    }

    @Test
    public void shouldReturnTheSpecifiedTeam() throws Exception{
        String teamId = "teamId";

        Team expectedTeam = new Team("teamId1", "teamName", Collections.emptySet());
        TeamDTO expectedTeamDTO = TeamDTO.convertToDTO(expectedTeam);

        when(teamService.getTeam(teamId)).thenReturn(expectedTeam);

        ObjectWriter objectWriter = new ObjectMapper().writer().withDefaultPrettyPrinter();

        String jsonOutput = objectWriter.writeValueAsString(expectedTeamDTO);

        mockMvc.perform(get("/teams/" + teamId))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().json(jsonOutput));

    }

    @Test
    public void shouldAddTeam() throws Exception{
        TeamDTO addTeamRequest = new TeamDTO();
        addTeamRequest.setName("Temeta");
        HashSet<String> userIds = new HashSet<>();
        userIds.add("yakam"); userIds.add("sughram"); userIds.add("daya");
        addTeamRequest.setUserIds(userIds);

        Team expectedTeam = new Team("teamId1", "teamName", Collections.emptySet());
        TeamDTO expectedTeamDTO = TeamDTO.convertToDTO(expectedTeam);

        when(teamService.addTeam(addTeamRequest)).thenReturn(expectedTeam);

        ObjectWriter objectWriter = new ObjectMapper().writer().withDefaultPrettyPrinter();

        String jsonOutput = objectWriter.writeValueAsString(expectedTeamDTO);


        mockMvc.perform(post("/teams").
                contentType(TestUtils.APPLICATION_JSON_UTF8).
                content(TestUtils.convertObjectToJsonBytes(addTeamRequest))).
                andExpect(status().isOk())
                .andExpect(content().json(jsonOutput));
    }

    @Test
    public void shouldDeleteTeamIfExists() throws Exception{
        when(teamService.deleteTeam("1234")).thenReturn(true);
        mockMvc.perform(delete("/teams/1234"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().string(containsString("Success!")));

    }

    @Test
    public void shouldNotDeleteIfNotExists() throws Exception{
        when(teamService.deleteTeam("1234")).thenReturn(false);
        mockMvc.perform(delete("/teams/1234"))
                .andDo(print())
                .andExpect(status().isBadRequest())
                .andExpect(content().string(containsString("Failed!")));
    }

    @Test
    public void shouldUpdateName() throws Exception{
        Team expectedTeam = new Team("52","Heimdall", new HashSet<>());
        Team updatedTeam = new Team("52", "NewName", new HashSet<>());
        TeamDTO expectedDTO = TeamDTO.convertToDTO(expectedTeam);
        when(teamService.updateTeamName(new TeamDTO("52", "Heimdall", new HashSet<>()), "52")).thenReturn(expectedTeam);

        mockMvc.perform(put("/teams/52")
        .contentType(MediaType.APPLICATION_JSON_UTF8)
        .content(jsonify(updatedTeam)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.teamId").exists())
                .andExpect(jsonPath("$.name").value("Heimdall"));
//    }

    }

    @Test
    public void shouldAddToTeamTestSuccesfully() throws Exception {
        when(teamService.addToTeamRequest("1", new ArrayList<>())).thenReturn(true);
        mockMvc.perform(post("/teams/1/assignTeamMember")
                .contentType(TestUtils.APPLICATION_JSON_UTF8)
                .content(TestUtils.convertObjectToJsonBytes(new ArrayList<>())))
                .andExpect(status().isAccepted())
                .andExpect(content().string(containsString("Success!")));
    }

    @Test
    public void shouldAddToTeamTestFailure() throws Exception {
        when(teamService.addToTeamRequest("1", new ArrayList<>())).thenReturn(false);
        mockMvc.perform(post("/teams/1/assignTeamMember")
                .contentType(TestUtils.APPLICATION_JSON_UTF8)
                .content(TestUtils.convertObjectToJsonBytes(Arrays.asList("user1", "user2"))))
                .andExpect(status().isBadRequest())
                .andExpect(content().string(containsString("Failed!")));
    }


}
