package com.example.demo;

import com.example.demo.entity.Team;
import com.example.demo.entity.TeamDTO;
import com.example.demo.repository.TeamRepository;
import com.example.demo.service.TeamServiceImpl;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.HashSet;
import java.util.List;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.*;
import static org.mockito.Mockito.when;

@RunWith(SpringRunner.class)
public class TeamServiceImplTest {

    @InjectMocks
    TeamServiceImpl teamService;

    @Mock
    TeamRepository teamRepository;

    @Before
    public void setUp() {
        teamService = new TeamServiceImpl(teamRepository);
    }

    @After
    public void tearDown() {
        teamRepository = null;
        teamService = null;
    }
    @Test
    public void getTeamsTestWhenEmptyList(){
        List<Team> testList = teamService.getTeams();
        assertTrue(testList.isEmpty());
    }

    @Test
    public void getTeamTest(){
        Team expectedTeam = new Team("id", "Name",new HashSet<>());
        when(teamRepository.getTeam("id")).thenReturn(expectedTeam);
        Team receivedTeam = teamService.getTeam("id");
        assertEquals(expectedTeam, receivedTeam);
    }

    @Test
    public void updateTeamTest(){
        Team oldTeam = new Team("1", "oldName", new HashSet<>());
        Team newTeam = new Team("1", "newTeam", new HashSet<>());
        when(teamRepository.updateTeamName(new TeamDTO(), oldTeam.getTeamId())).thenReturn(newTeam);
        assertEquals(teamService.updateTeamName(new TeamDTO(), oldTeam.getTeamId()).getName(), newTeam.getName());
    }

    @Test
    public void addToTeamTest() {
        Team team = new Team("1", "one", new HashSet<>());
        when(teamRepository.addUserToTeam("1", null)).thenReturn(team);
        assertThat(team, is(teamRepository.addUserToTeam("1", null)));
    }

}
