package com.example.demo;

import com.amazonaws.services.dynamodbv2.AmazonDynamoDB;
import com.amazonaws.services.dynamodbv2.document.Item;
import com.amazonaws.services.dynamodbv2.document.PutItemOutcome;
import com.amazonaws.services.dynamodbv2.document.Table;
import com.amazonaws.services.dynamodbv2.model.PutItemResult;
import com.amazonaws.services.dynamodbv2.model.ScanRequest;
import com.example.demo.entity.Team;
import com.example.demo.repository.TeamRepositoryDBImpl;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.HashSet;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class TeamRepositoryDBImplTest {

    @Mock
    Table table;
    @Mock
    AmazonDynamoDB client;
    @Mock
    ScanRequest scan;

    @InjectMocks
    TeamRepositoryDBImpl repository;

    @Test
    public void addToListTest() {
        Team team = new Team("1", "one", new HashSet<>());
        team.getUserIds().add("Haha");
        Item item = new Item();
        item.withPrimaryKey("teamId", team.getTeamId());

        //Checking null and empty conditions for added team
        if (team.getName() != null && !team.getName().equals("")) {
            item.withString("name", team.getName());
        }

        if (team.getUserIds() != null && !team.getUserIds().isEmpty()) {
            item.withStringSet("userIds", team.getUserIds());
        }

        when(table.putItem(item)).thenReturn(new PutItemOutcome(new PutItemResult()));
        assertThat(team, is(repository.addToList(team)));
    }

//    @Test
//    public void getTeamListTest(){
//        List<Team> listTeam = new ArrayList<>(Arrays.asList(new Team("1", "one", new HashSet<>())
//                                            , new Team("2", "two", new HashSet<>())));
//        when(new ScanRequest().withTableName("Onboarding-Team")).thenReturn(null);
//        when(client.scan(scan)).thenReturn(null);
//        assertThat()
//    }

}
