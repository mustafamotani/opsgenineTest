package com.example.demo.controller;

import com.example.demo.entity.TeamDTO;
import com.example.demo.service.TeamService;
import com.fasterxml.jackson.core.JsonProcessingException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(value = "/teams")
public class TeamRestController {

    @Autowired
    TeamService teamService;

    @RequestMapping(method = RequestMethod.GET)
    public ResponseEntity<List<TeamDTO>> getTeams() {
        List<TeamDTO> teamlist = TeamDTO.convertAllToDTO(teamService.getTeams());
        return ResponseEntity.status(HttpStatus.OK).body(teamlist);
    }

    @RequestMapping(method = RequestMethod.GET, value = "/{teamId}")
    public ResponseEntity<TeamDTO> getTeamFromId(@PathVariable String teamId) {
        TeamDTO teamdto = TeamDTO.convertToDTO(teamService.getTeam(teamId));
        return ResponseEntity.status(HttpStatus.OK).body(teamdto);
    }

    @RequestMapping(method = RequestMethod.POST)
    public ResponseEntity<TeamDTO> addTeam(@RequestBody TeamDTO teamdto) {
        TeamDTO latestTeamdto = TeamDTO.convertToDTO(teamService.addTeam(teamdto));
        return ResponseEntity.status(HttpStatus.OK).body(latestTeamdto);
    }

    @RequestMapping(method = RequestMethod.PUT, value = "/{teamId}")
    public ResponseEntity<TeamDTO> updateTeam(@RequestBody TeamDTO teamdto, @PathVariable String teamId) {
        TeamDTO latestTeamdto = TeamDTO.convertToDTO(teamService.updateTeamName(teamdto, teamId));
        return ResponseEntity.status(HttpStatus.OK).body(latestTeamdto);
    }

    @RequestMapping(method = RequestMethod.DELETE, value = "/{teamId}")
    public ResponseEntity deleteTeam(@PathVariable String teamId) {
        if (teamService.deleteTeam(teamId)) {
            return ResponseEntity.status(HttpStatus.OK).body("Success!");
        }
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Failed!");
    }

    @RequestMapping(method = RequestMethod.POST, value = "/{teamId}/assignTeamMember")
    public ResponseEntity addToTeam(@RequestBody List<String> userIds, @PathVariable String teamId) {
        try {
            boolean isAccepted = teamService.addToTeamRequest(teamId, userIds);
            if (isAccepted)
                return ResponseEntity.status(HttpStatus.ACCEPTED).body("Success!");
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Failed!");
    }
}
