package com.example.demo.repository;

import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.AWSCredentialsProvider;
import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.services.sqs.AmazonSQS;
import com.amazonaws.services.sqs.AmazonSQSClientBuilder;
import com.amazonaws.services.sqs.model.Message;
import com.example.demo.service.SQSMessage;
import com.example.demo.service.TeamService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Component
public class ReceivedSQSMessagesProcessor {

    @Autowired
    private TeamService teamService;

    private AWSCredentials awsCredentials;
    private AWSCredentialsProvider awsCredentialsProvider;
    private AmazonSQS client;
    @Value("${sqs.url:https://sqs.eu-west-1.amazonaws.com/822610072828/team_assign_team_member_queue}")
    private String url;
    @Value("${db.region:eu-west-1}")
    private String region;
    @Value("${accessKey:AKIAJVDXFDAKRSC4X5KQ}")
    private String accessKey;
    @Value("${secretKey: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}")
    private String secretKey;
    @Value("${queue.name:team_assign_team_member_queue}")
    private String queueName;
    private String queueUrl;

    @PostConstruct
    void postConstruct() {
        awsCredentials = new BasicAWSCredentials(accessKey, secretKey);
        awsCredentialsProvider = new AWSStaticCredentialsProvider(awsCredentials);
        client = AmazonSQSClientBuilder.standard()
                .withCredentials(awsCredentialsProvider)
                .withRegion(region)
                .build();

        queueUrl = client.getQueueUrl(queueName).getQueueUrl();
    }

    @Scheduled(fixedDelay = 1000)
    public void receiveMessage() throws IOException {
        List<Message> messages = client.receiveMessage(queueUrl).getMessages();
        for (Message m : messages) {
            String body = m.getBody();
            SQSMessage sqsMessage = SQSMessage.stringToJson(body);
            teamService.addToTeam(sqsMessage.getTeamId(), sqsMessage.getUserId());
            client.deleteMessage(queueUrl, m.getReceiptHandle());
        }
    }

    public Map<String, String> stringToMap(String value) {
        value = value.substring(1, value.length() - 1);
        String[] keyValuePairs = value.split(",");
        Map<String, String> map = new HashMap<>();

        for (String pair : keyValuePairs) {
            String[] entry = pair.split(":");
            map.put(entry[0].trim(), entry[1].trim());
        }
        return map;
    }

}
