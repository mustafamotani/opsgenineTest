package com.example.demo.repository;

import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.AWSCredentialsProvider;
import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.client.builder.AwsClientBuilder;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDB;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDBClientBuilder;
import com.amazonaws.services.dynamodbv2.document.DynamoDB;
import com.amazonaws.services.dynamodbv2.document.Item;
import com.amazonaws.services.dynamodbv2.document.PrimaryKey;
import com.amazonaws.services.dynamodbv2.document.Table;
import com.amazonaws.services.dynamodbv2.model.*;
import com.example.demo.entity.Team;
import com.example.demo.entity.TeamDTO;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Repository;

import javax.annotation.PostConstruct;
import java.util.*;

@Repository
@Qualifier("TeamRepositoryDBImpl")
public class TeamRepositoryDBImpl implements TeamRepository {

    private AWSCredentials awsCredentials;
    private AWSCredentialsProvider awsCredentialsProvider;
    @Value("${db.url:http://localhost:3966}")
    private String url;
    @Value("${db.region:us-west-2}")
    private String region;
    @Value("${accessKey:fake}")
    private String accessKey;
    @Value("${secretKey:fake}")
    private String secretKey;

    private AmazonDynamoDB client;
    private DynamoDB dynamodb;
    private Table table;

    @PostConstruct
    void postConstruct() {
        awsCredentials = new BasicAWSCredentials(accessKey, secretKey);
        awsCredentialsProvider = new AWSStaticCredentialsProvider(awsCredentials);
        client = AmazonDynamoDBClientBuilder.standard().withEndpointConfiguration(
                new AwsClientBuilder.EndpointConfiguration(url, region))
                .withCredentials(awsCredentialsProvider)
                .build();
        dynamodb = new DynamoDB(client);
        table = dynamodb.getTable("Onboarding-Team");
    }


    @Override
    public Team addToList(Team team) {
        Item item = new Item();
        item.withPrimaryKey("teamId", team.getTeamId());

        //Checking null and empty conditions for added team
        if (team.getName() != null && !team.getName().equals("")) {
            item.withString("name", team.getName());
        }

        if (team.getUserIds() != null && !team.getUserIds().isEmpty()) {
            item.withStringSet("userIds", team.getUserIds());
        }

        table.putItem(item);
        return team;
    }

    @Override
    public List<Team> getTeamList() {
        List<Team> teamList = new ArrayList<>();
        ScanRequest scanRequest = new ScanRequest()
                .withTableName("Onboarding-Team");

        ScanResult result = client.scan(scanRequest);

        for (Map<String, AttributeValue> item : result.getItems()) {
            String id = item.get("teamId").getS();
            String name = "";
            if (item.get("name") != null)
                name = item.get("name").getS();
            Set<String> userIds = new HashSet<>();
            if (item.get("userIds") != null)
                userIds = new HashSet<>(item.get("userIds").getSS());
            teamList.add(new Team(id, name, userIds));
        }

        return teamList;
    }

    @Override
    public boolean deleteTeam(String teamId) {
        if (getTeam(teamId) == null)
            return false;
        table.deleteItem(new PrimaryKey("teamId", teamId));
        return true;
    }

    @Override
    public Team getTeam(String id) {
        Item item = table.getItem(new PrimaryKey("teamId", id));
        if (item != null)
            return new Team(item.getString("teamId"), item.getString("name"), item.getStringSet("userIds"));
        return null;
    }

    @Override
    public Team updateTeamName(TeamDTO teamdto, String teamId) {
        Team team = getTeam(teamId);
        if (team == null)
            return null;
        UpdateItemRequest updateItemRequest = new UpdateItemRequest()
                .withTableName("Onboarding-Team")
                .addKeyEntry("teamId", new AttributeValue().withS(teamId))
                .addAttributeUpdatesEntry("name",
                        new AttributeValueUpdate().withValue(new AttributeValue().withS(teamdto.getName())));
        client.updateItem(updateItemRequest);
        return getTeam(teamId);
    }

    @Override
    public Team addUserToTeam(String teamId, String userId) {
        Team team = getTeam(teamId);
        if (team == null)
            return null;
        team.addToTeam(userId);
        UpdateItemRequest updateItemRequest = new UpdateItemRequest()
                .withTableName("Onboarding-Team")
                .addKeyEntry("teamId", new AttributeValue().withS(teamId))
                .addAttributeUpdatesEntry("userIds",
                        new AttributeValueUpdate().withValue(new AttributeValue().withSS(team.getUserIds())));
        client.updateItem(updateItemRequest);
        return getTeam(teamId);
    }
}
