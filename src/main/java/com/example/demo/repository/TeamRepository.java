package com.example.demo.repository;

import com.example.demo.entity.Team;
import com.example.demo.entity.TeamDTO;

import java.util.List;

public interface TeamRepository {

    Team addToList(Team team);

    List<Team> getTeamList();

    boolean deleteTeam(String teamId);

    Team getTeam(String id);

    Team updateTeamName(TeamDTO teamdto, String teamId);

    Team addUserToTeam(String teamId, String userId);
}
