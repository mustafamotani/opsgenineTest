package com.example.demo.repository;

import com.example.demo.entity.Team;
import com.example.demo.entity.TeamDTO;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

@Repository
//@Qualifier("TeamRepositoryImpl")
public class TeamRepositoryImpl implements TeamRepository {

    private List<Team> teamList;

    public TeamRepositoryImpl() {
        teamList = new ArrayList<>();
    }

    public TeamRepositoryImpl(List<Team> teamList) {
        this.teamList = teamList;
    }

    public List<Team> getTeamList() {
        return teamList;
    }

    public void setTeamList(List<Team> teamList) {
        this.teamList = teamList;
    }

    public Team addToList(Team team) {
        for (Team current : teamList) {
            if (team.equals(current))
                return current;
        }
        teamList.add(team);
        return team;
    }

    public Team getTeam(String teamId) {
        Iterator<Team> iterator = getTeamList().iterator();
        while (iterator.hasNext()) {
            Team temp = iterator.next();
            if (temp.getTeamId().equals(teamId))
                return temp;
        }
        return null;
    }

    public boolean deleteTeam(String teamId) {
        Team team = getTeam(teamId);
        if (team != null) {
            getTeamList().remove(team);
            return true;
        }
        return false;
    }

    public Team updateTeamName(TeamDTO teamdto, String teamId) {
        Team team = getTeam(teamId);
        if (team != null) {
            team.setName(teamdto.getName());
            return team;
        }
        return null;
    }

    @Override
    public Team addUserToTeam(String teamId, String userId) {
        return null;
    }
}
