package com.example.demo.repository;

import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.AWSCredentialsProvider;
import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.services.sqs.AmazonSQS;
import com.amazonaws.services.sqs.AmazonSQSClientBuilder;
import com.amazonaws.services.sqs.model.SendMessageRequest;
import com.example.demo.service.SQSMessage;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

@Component
public class SQSQueueImpl {
    private AWSCredentials awsCredentials;
    private AWSCredentialsProvider awsCredentialsProvider;
    private AmazonSQS client;

    @Value("${sqs.url:https://sqs.eu-west-1.amazonaws.com/822610072828/team_assign_team_member_queue}")
    private String url;
    @Value("${db.region:eu-west-1}")
    private String region;
    @Value("${accessKey:AKIAJVDXFDAKRSC4X5KQ}")
    private String accessKey;
    @Value("${secretKey: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}")
    private String secretKey;
    @Value("${queue.name:team_assign_team_member_queue}")
    private String queueName;
    private String queueUrl;

    @PostConstruct
    void postConstruct() {
        awsCredentials = new BasicAWSCredentials(accessKey, secretKey);
        awsCredentialsProvider = new AWSStaticCredentialsProvider(awsCredentials);
        client = AmazonSQSClientBuilder.standard()
                .withCredentials(awsCredentialsProvider)
                .withRegion(region)
                .build();

        queueUrl = client.getQueueUrl(queueName).getQueueUrl();
    }

    public String jsonify(Object o) throws JsonProcessingException {
        com.fasterxml.jackson.databind.ObjectWriter objectWriter = new ObjectMapper().writer().withDefaultPrettyPrinter();
        String jsonOutput = objectWriter.writeValueAsString(o);
        return jsonOutput;
    }

    public void sendMessage(SQSMessage message) throws JsonProcessingException {
        SendMessageRequest send_msg_request = new SendMessageRequest()
                .withQueueUrl(queueUrl)
                .withMessageBody(jsonify(message));
        client.sendMessage(send_msg_request);
    }

}
