package com.example.demo.entity;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.*;

public class TeamDTO {

    @JsonProperty("teamId")
    private String teamId;

    @JsonProperty("name")
    private String name;

    @JsonProperty("userIds")
    private Set<String> userIds;


    public String getTeamId() {
        return teamId;
    }


    public String getName() {
        return name;
    }

    public Set<String> getUserIds() {
        return userIds;
    }

    public void setUserIds(Set<String> userIds){
        this.userIds = userIds;
    }

    public static TeamDTO convertToDTO(Team team){
        if (team == null)
            return null;
        TeamDTO teamdto = new TeamDTO();
        teamdto.teamId = team.getTeamId();
        teamdto.name = team.getName();
        teamdto.userIds = team.getUserIds();
        return teamdto;
    }

    public TeamDTO(){}

    public TeamDTO(Team team){
        this.teamId = team.getTeamId();
        this.name = team.getName();
        this.userIds = team.getUserIds();
    }

    public static List<TeamDTO> convertAllToDTO(List<Team> listTeam){
        if (listTeam.isEmpty())
            return new ArrayList<TeamDTO>();
        List<TeamDTO> result = new ArrayList<>();
        Iterator<Team> teamIterator = listTeam.iterator();
        while(teamIterator.hasNext()) result.add(convertToDTO(teamIterator.next()));
        return result;
    }

    public void addToTeam(String userId) {
        userIds.add(userId);
    }

    public TeamDTO(String teamId, String name, Set<String> userIds) {
        this.teamId = teamId;
        this.name = name;
        this.userIds = userIds;
    }

    public void setName(String name){

        this.name = name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TeamDTO teamDTO = (TeamDTO) o;
        return Objects.equals(teamId, teamDTO.teamId);
    }

    @Override
    public int hashCode() {

        return Objects.hash(teamId);
    }
}
