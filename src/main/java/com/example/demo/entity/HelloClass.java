package com.example.demo.entity;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HelloClass {

    @RequestMapping(value = "/hello", method = RequestMethod.GET)
    public String printHello(){
        return "Hello World";
    }

    @RequestMapping(value = "/ok", method = RequestMethod.GET)
    public ResponseEntity itIsOk(){
        return ResponseEntity.status(HttpStatus.OK).body("Hello World");
    }
}
