package com.example.demo.entity;

import java.util.Set;

public class Team {

    private String teamId;
    private String name;
    private Set<String> userIds;

    public String getTeamId() {
        return teamId;
    }

    public void setTeamId(String teamId) {
        this.teamId = teamId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Set<String> getUserIds() {
        return userIds;
    }

    public void setUserIds(Set<String> userIds) {
        this.userIds = userIds;
    }

    public Team(String teamId, String name, Set<String> userIds) {
        this.teamId = teamId;
        this.name = name;
        this.userIds = userIds;
    }

    public boolean equals (Team team){
        if (this.teamId.equals(team.teamId))
            return true;
        return false;
    }

    public void addToTeam(String u){
        userIds.add(u);
    }
}
