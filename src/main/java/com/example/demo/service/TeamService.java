package com.example.demo.service;

import com.example.demo.entity.Team;
import com.example.demo.entity.TeamDTO;
import com.fasterxml.jackson.core.JsonProcessingException;

import java.util.List;

public interface TeamService {

    List<Team> getTeams();
    Team getTeam(String id);
    Team addTeam(TeamDTO teamdto);
    Team updateTeamName(TeamDTO teamdto, String teamId);
    boolean deleteTeam(String teamId);

    boolean addToTeamRequest(String teamId, List<String> userId) throws JsonProcessingException;

    Team addToTeam(String teamId, String userId);
}
