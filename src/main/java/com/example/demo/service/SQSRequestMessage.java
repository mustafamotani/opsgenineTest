package com.example.demo.service;

import com.fasterxml.jackson.annotation.JsonProperty;

public class SQSRequestMessage {

    @JsonProperty
    private String userIds;

    public String getUserIds() {
        return userIds;
    }
}
