package com.example.demo.service;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;


public class SQSMessage {

    @JsonProperty
    private String teamId;
    @JsonProperty
    private String userId;

    public SQSMessage(String teamId, String userId) {
        this.teamId = teamId;
        this.userId = userId;
    }

    public String getTeamId() {
        return teamId;
    }

    public String getUserId() {
        return userId;
    }

    public SQSMessage() {
        this.teamId = "";
        this.userId = "";
    }

    public static SQSMessage stringToJson(String str) throws IOException {
        return new ObjectMapper().readValue(str, SQSMessage.class);
    }

}
