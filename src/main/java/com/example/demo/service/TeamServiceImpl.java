package com.example.demo.service;

import com.example.demo.entity.Team;
import com.example.demo.entity.TeamDTO;
import com.example.demo.repository.SQSQueueImpl;
import com.example.demo.repository.TeamRepository;
import com.fasterxml.jackson.core.JsonProcessingException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.UUID;

@Service
public class TeamServiceImpl implements TeamService {

    @Autowired
    @Qualifier("TeamRepositoryDBImpl")
    private TeamRepository teamRepository;
    @Autowired
    private SQSQueueImpl sqsQueue;

    @Autowired
    public TeamServiceImpl(@Qualifier(value = "TeamRepositoryDBImpl") TeamRepository teamRepository) {
        this.teamRepository = teamRepository;
    }

    public List<Team> getTeams(){
        return teamRepository.getTeamList();
    }

    public Team getTeam(String id){
        return teamRepository.getTeam(id);
    }

    public Team addTeam(TeamDTO teamdto){
        Team team = new Team(UUID.randomUUID().toString().replace("-", ""), teamdto.getName(), teamdto.getUserIds());
        return teamRepository.addToList(team);
    }

    public Team updateTeamName(TeamDTO teamdto, String teamId){
        return teamRepository.updateTeamName(teamdto, teamId);
    }

    public boolean deleteTeam(String teamId){
        return teamRepository.deleteTeam(teamId);
    }

    public boolean addToTeamRequest(String teamId, List<String> userIds) throws JsonProcessingException {
        Team team = getTeam(teamId);
        if (team == null)
            return false;

        for (String userId : userIds)
            sqsQueue.sendMessage(new SQSMessage(teamId, userId));
        return true;
    }

    public Team addToTeam(String teamId, String userId) {
        return teamRepository.addUserToTeam(teamId, userId);
    }

}
